import { Navigate, Route, Routes } from 'react-router-dom';
import { ChatBoot } from '../components/ChatBoot';

export const ChatBootRouter = () => {
    return (
      <>
        <Routes>    
            <Route path="/chat" element={<ChatBoot />} />
        </Routes>    
      </>
    )
}

