import React from 'react';
import { createRoot } from "react-dom/client";
import "./index.css";
import App from "./App";
// import "bootstrap/dist/css/bootstrap.min.css";

const container = document.getElementById("root");
const root = createRoot(container);

root.render(<App />);



// // import FirstPerson from './components/FirstPerson';
// // import SecondPerson from './components/SecondPerson';
// import { createRoot } from 'react-dom/client'
// // import { Boot } from './components/Boot';


// import './index.css';
// import App from './App';

// import { Routes, Route, BrowserRouter } from 'react-router-dom';
// import { ChatBoot } from './components/ChatBoot';
// import { Saludo } from './components/Saludo';
// import Switcher from './components/Switcher';



// const container = document.getElementById('root');
// const root = createRoot(container);
// root.render(
//   <BrowserRouter>
//     <Routes>
//       <Switcher/>
//       <Saludo />
//       <ChatBoot/>
//     </Routes>
//   </BrowserRouter>
// )



// import React from 'react';
// import ReactDOM from 'react-dom/client';
// import App from './App';
// import reportWebVitals from './reportWebVitals';
// import { store } from './store';
// import { Provider } from 'react-redux';

// const root = ReactDOM.createRoot(
//   document.getElementById('root') as HTMLElement
// );
// root.render(
//   <React.StrictMode>
//      <Provider store={store}>
//       <App />
//      </Provider>
    
//   </React.StrictMode>
// );

// // If you want to start measuring performance in your app, pass a function
// // to log results (for example: reportWebVitals(console.log))
// // or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
