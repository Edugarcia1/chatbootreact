import ReactDOM from "react-dom";
import { useState } from 'react';
import { ChatBoot }  from './ChatBoot';
// import { useNavigate} from 'react-router-dom';
import { DirectLine } from 'botframework-directlinejs';
import * as env from './bot/common';
// import { ChatBoot } from "./ChatBoot";

// Obteniendo estilos de adaptive cards
const adaptiveCardsHostConfig = this.token_resp.adaptiveCardsHostConfig;
// const navigate = useNavigate();

// VERSION 2    
const styleOptions = {
    backgroundColor: '#FFFFFF',
      
    // Caja de escritura
            hideUploadButton: true,                               // Ocultar Clip de adjuntar
            sendBoxBackground: 'rgb(232, 236, 237)',
            sendBoxButtonColor: 'rgb(127, 127, 127)',               // Color del botón de envío
            sendBoxButtonColorOnHover: "rgb(255, 51, 51)",
            sendBoxTextColor: 'rgb(0, 0, 0) !important',                     // Color de texto de escritura
            sendBoxPlaceholderColor: 'rgb(127, 127, 127) !important',              // Color de texto de ejemplo de la caja de escritura
      
            // Burbujas de conversacion ::: BOT
            bubbleBackground: 'linear-gradient(to bottom, rgb(208,226,248),  rgb(204,238,254));',
            bubbleBorderRadius: 18,
            bubbleBorderColor: 'rgb(205,209,209)',
            bubbleBorderWidth: '1px',
      
            // Burbujas de conversacion :::  USUARIO
            bubbleFromUserBackground: 'rgb(252,253,253) !important',
            bubbleFromUserBorderRadius: 18,
            bubbleFromUserBorderColor: 'rgb(205,209,209)',
            bubbleFromUserBorderWidth: '1px',
      
            // Avatares
            avatarSize: 35,
            botAvatarBackgroundColor: "FFFFFF",
            botAvatarImage: env.BOTconf.botAvatarImage,
            botAvatarInitials: '',
            userAvatarBackgroundColor: "#FFFFFF",
            userAvatarImage: env.BOTconf.userAvatarImage,
            userAvatarInitials: '',
      
            bubbleNubOffset: 'bottom',
            bubbleFromUserNubOffset: 'bottom',
      
            // Mensaje de just now, sending...
            subtle: 'rgb(0, 0, 0)',
            typingAnimationBackgroundImage: "url('data:image/gif;base64,R0lGODlhXwAhAPEAAAAAACc6mUep1lin0yH5BAkKAAAAIf8LTkVUU0NBUEUyLjADAQAAACwAAAAAXwAhAAACz4SPqcvtD6OctNqLs968+w+G4kiW5ok2wcq2LvsMwkzX9jDE9l7j1gsENnC8Ii7HIBZ3x0rwCUsujciFbMqrSqDQBtbo/fKc3GBY3DujaeTyS432Mda2trsFj+fR9vtq/6WVQFfT5weIJYhAyEbh54K4JLfAOGN4FymZuXTptpn1Oeb4GEXJqHhQKdBZFtqDaqDKyuWaNlc523VLCAsgO0oaUEvT+zsR/Ce11qS83MuArBP3DHAlNgl86KAUCMGdmBIuPk5ebn6Onq6+zp5RAAAh+QQJCgAAACwAAAAAXwAhAAAC0ISPqcvtD6OctNqLs968+w+G4kiW5ok2g8C27jsMT0DX9l0/68u3sRXrCWMyBu54VO2EsB+FyXQukNQcYwmFVbLDRrXa4Aq3Yq3xiwyXX+S1QKpApxluV9tdnMpx6rrgvpYXt3fTVwdYJphAWEjn9/dUB7fIaLXwCDnxqIhQaamAiSjGeeBJY+gmykVqYBqAuqYKNdlpClsmO+tl6+iXG7XreSv223Um3HtIgbVKW8q7wNwMFMjaCh1dfRE0CoGslJgiPk5ebn6Onq6+zt6eUQAAIfkECQoAAAAsAAAAAF8AIQAAAtCEj6nL7Q+jnLTai7PevPsPhuJIluaJNoPAtu47DM/61m38BPrO97sVswljMkZQWCM2fExmBYnEGaE26aKJ/VGowwa3usxin1+Yt+wqXsVNMlpgVbxZ8QS7vZ3Dz3O14u6U18f35mcH2ONWSIhmiICYKNjoeKBX9wipNaG3x8BJaZCpKfHJWAYKIKqj2Gj6haoawPqKagmrOvvqynUrmsu7S9Wb+SscDDUMWUFzennATJuMCDQ5U+2AuwyNDHEEnEOcIj5OXm5+jp6uvs7eflEAADs=')",
            typingAnimationHeight: 33,
            typingAnimationWidth: 95,
}

const close = () =>{
    console.log("Para cerrar el boot");
    // navigate(-1);
    // navigate('/',{
    //     replace:true
    // });

    // Cierra el bot 
    this.bot.enviar_evento_stop_speech();

    //Se habilita nuevamente el botón de llamada al bot.
    this.saludoElement.nativeElement.style.opacity = 1;
    this.saludoElement.nativeElement.style.visibility = "visible";
    this.mostrar_saludo = !this.mostrar_saludo;

    //Contenedor del chat
    this.chatElement.nativeElement.style.opacity = 0;
    this.chatElement.nativeElement.style.visibility = "hidden";

    // Encabezado
    this.encabezadoElement.nativeElement.style.opacity = 0;
    this.encabezadoElement.nativeElement.style.visibility = "hidden";

    // Cuerpo del bot
    this.bot.botWindowElement.nativeElement.style.opacity = 0;
    this.bot.botWindowElement.nativeElement.style.visibility = "hidden";
}

const openDialogCaritometro = () =>{
    console.log("Para Dialogo Carimetro");
    /*this.dialog.open(CaritometroChatBotComponent, {
    maxHeight: '600px',
    maxWidth: '700px'
    });*/
}

const cambiarIdioma = () =>{
    console.log("Para cambiar idioma");
    if(localStorage.localeBot === 'es-mx') {
    localStorage.localeBot = 'en-us'
    this.changeLocale = 'ES'
    this.bot.start()
    } else {
    localStorage.localeBot = 'es-mx'
    this.changeLocale = 'EN'
    this.bot.start()
    }
}

function Saludo() {
    // let entra = false;
    const [opened, setOpened] = useState(false);
   
    return (
        <>
            <div className="switcher-div">
                <div className='saludo_bot'>
                    <div className="bot_mensaje">Hola soy TECbot, ¿en qué te puedo ayudar?</div>
                    {/* <img src="imgChat"/> */}
                    <button className="bot" onClick={ ()=>setOpened(!opened) } />
                    {/* <button className="bot" onClick={ ()=>setOpened(opened) } /> */}
                </div>
            </div>
    
            {opened && (
                <>
                    <p>ABRE CHATBOT</p>
                </>
            )}

        </>
    );

}

export {Saludo};