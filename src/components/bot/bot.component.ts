// import { Component, ElementRef, OnInit, ViewChild, AfterViewInit, HostListener } from '@angular/core';
// import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as env from '../bot/common'
import { Observable, timer } from 'rxjs';
import { DirectLine } from 'botframework-directlinejs';

declare global {
  interface Window {
    WebChat: any;
  }
}
window.WebChat = window.WebChat || {};

// @Component({
//   selector: 'app-bot',
//   templateUrl: './bot.component.html',
//   styleUrls: ['./bot.component.scss']
// })
// export class BotComponent implements OnInit, AfterViewInit {

//   @ViewChild("botWindow") botWindowElement: ElementRef;

//   @HostListener('window:resize', ['$event'])
//   onResize(event) {
//     this.checkOrientation();
//   }
//   @HostListener('document:bot_stop_speech', ['$event'])
//   onStopSpeech(event) {
//     this.enviar_evento_stop_speech();
//   }

//   constructor(private http: HttpClient) { }

//   info = undefined;
//   data = undefined;
//   dominio = undefined;
//   dominioAPI = undefined;
//   developer = false;
//   svg_send = "M2.01,3 L2,10 L17,12 L2,14 L2.01,21 L23,12 L2.01,3 Z M4,8.25 L4,6.03 L11.51,9.25 L4,8.25 Z M4.01,17.97 L4.01,15.75 L11.52,14.75 L4.01,17.97 Z";


//   disp_movil = undefined;
//   ngAfterViewInit() {
//     // Verificando desde donde se está cargando el chatbot
//     if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
//       this.disp_movil = true;

//     } else {
//       this.disp_movil = false;
//     }
//     this.checkOrientation();
//     // window.addEventListener("resize", this.checkOrientation, false);
//     // window.addEventListener("orientationchange", this.checkOrientation, false);
//   }

//   checkOrientation() {
//     let altura_pantalla = screen.height;
//     // console.log("###>>> Altura pantalla: " + altura_pantalla)
//     let altura_enc_navegador = screen.height - window.innerHeight;
//     let altura_env_mitec = undefined;

//     // Validando que contenga TAG NAV
//     if (document.getElementsByTagName("nav").length == 0) {
//       altura_env_mitec = 72;
//     }
//     else {
//       altura_env_mitec = document.getElementsByTagName("nav")[0].offsetHeight;
//     }

//     // Ajustando para dispositivos
//     if (this.disp_movil == true) {
//       // altura_env_mitec = 110;  // dispositivo movil
//       altura_env_mitec = altura_env_mitec + 35;  // dispositivo movil
//     }
//     else {
//       // altura_env_mitec = 130;  // dispositivo escritorio
//       altura_env_mitec = altura_env_mitec + 60;  // escritorio
//     }
//     let altura_calc = altura_pantalla - (altura_enc_navegador + altura_env_mitec);
//     // console.log("###>>> Altura calculada: " + altura_calc)
//     if (altura_calc > 600) {
//       altura_calc = 600
//     }
//     let str_calc = altura_calc.toString() + "px";
//     this.botWindowElement.nativeElement.style.height = str_calc;
//   }

//   ngOnInit() {
//     // Validando ambiente
//     this.dominio = window.location.hostname;      //tecmx.sharepoint.com     //miespacio.itesm.mx     //mitec.itesm.mx    //mitecpprd.itesm.mx

//     if (this.dominio != "localhost") {
//       this.developer = false;
//     }
//     else {
//       this.developer = true;
//       //Indicador de ejecución local
//       console.log("ngOnInit - Corriendo Localmente");
//     }


//     // Validando dominio 
//     if (this.dominio == 'mitecpprd.itesm.mx') {
//       // Token de Bot ( v2.0 ) para desplegar en "Mitec PPRD"
//       this.dominioAPI = env.DominiosAPI.pprd
//     }
//     else if (this.dominio == 'mitec.itesm.mx') {
//       // Token de Bot ( v2.0 ) para desplegar en "Mitec PROD"
//       this.dominioAPI = env.DominiosAPI.prod
//     }
//     else {
//       // Token de Bot ( v2.0 ) para desplegar en "Sharepoint online" (por ejemplo)
//       this.dominioAPI = env.DominiosAPI.dev
//     }

//   }

//   start(): void {
//     //Indicador de inicio de respuestas del bot
//     this.developer == true ? console.log("Iniciando conversación con Bot") : "";
//     this.build_bot();
//   }

//   async call_api() {
//     // Llamar la api de sharepoint para obtener los datos del usuario
//     // Formando Api Sharepoint
//     let api = env.BOTconf.Sharepoint_API(this.dominio);

//     // Llamado a API
//     const promise = this.http.get(api).toPromise();

//     await promise.then((data) => {
//       this.info = data;
//       // Construyendo el json de envío
//       this.build_data(this.info)
//     }).catch((error) => {
//       //Revisa error en consola
//       console.log("Promise rejected with " + JSON.stringify(error));
//     });
//   }


//   build_data(json_): void {
//     // Obteniendo propiedades del usuario
//     this.data = {
//       "AccountName": this.info["AccountName"],
//       "DisplayName": this.info["DisplayName"],
//       "Email": this.info["Email"],
//       "ExtendedManagers": this.info["ExtendedManagers"] != undefined ? this.info["ExtendedManagers"].length > 1 ? "Sí" : "No" : "Indefinido",
//       "Title": this.info["Title"],
//       "UserProfile_GUID": this.info["UserProfileProperties"],
//       "UserName": undefined,
//       "FirstName": undefined,
//       "LastName": undefined,
//       "ITESMPersonPIDM": undefined,
//       "ITESMStAcadPlanID": undefined,
//       "ITESMStAcadPlanDesc": undefined,
//       "ITESMStACampusDesc": undefined,
//       "ITESMStAcadPlanNivel": undefined,
//       "ITESMStAcadPlanNivelDesc": undefined,
//       "ITESMStAEstatus": undefined,
//       "urlOrigen": window.location.href,
//       "urlOrigenPadre": window.parent.window.location.href
//     };

//     let vec_user_properties = this.info["UserProfileProperties"];
//     // Recorriendo para obtener los datos 
//     for (const index in vec_user_properties) {
//       let key = vec_user_properties[index].Key
//       let value = vec_user_properties[index].Value

//       switch (key) {
//         case "UserProfile_GUID": this.data["UserProfile_GUID"] = value;
//           break;
//         case "UserName": this.data["UserName"] = value
//           break;
//         case "FirstName": this.data["FirstName"] = value
//           break;
//         case "LastName": this.data["LastName"] = value
//           break;
//         case "ITESMPersonPIDM": this.data["ITESMPersonPIDM"] = value
//           break;
//         case "ITESMStAcadPlanID": this.data["ITESMStAcadPlanID"] = value
//           break;
//         case "ITESMStAcadPlanDesc": this.data["ITESMStAcadPlanDesc"] = value
//           break;
//         case "ITESMStACampusDesc": this.data["ITESMStACampusDesc"] = value
//           break;
//         case "ITESMStAcadPlanNivel": this.data["ITESMStAcadPlanNivel"] = value
//           break;
//         case "ITESMStAcadPlanNivelDesc": this.data["ITESMStAcadPlanNivelDesc"] = value
//           break;
//         case "ITESMStAEstatus": this.data["ITESMStAEstatus"] = value
//           break;
//       }
//     }
//   }

  // call_api_directline(correo): Observable<object> {
  //   const body = '{"id":"elidquesea","name": "elnombrequesea"}';
  //   const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  //   let api = env.BOTconf.DL_SecretToToken(this.dominioAPI)
  //   // Validar que api se está consultando
  //   return this.http.post(api, body, httpOptions)
  // }

  // call_api_speech(correo): Observable<object> {
  //   const body = '{}';
  //   const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  //   let api = env.BOTconf.DL_SpeechToken(this.dominioAPI)
  //   return this.http.post(api, body, httpOptions)
  // }

  // correo = undefined;
  // token_resp = undefined;

  // store = undefined;
  // directLine = undefined;

  // credentials = undefined;
  // webSpeechPonyfillFactory = undefined;
  // mute_cnt = 0;
  // boton_mute_creado = false;

  // async build_bot() {

  //   //############################################################################################
  //   if (this.developer == false) {
  //     await this.call_api();
  //     this.correo = this.data["Email"]
  //   }
  //   else {
  //     // En caso de que se corra localmente
  //     // ng serve --port 44307 --ssl
  //     this.correo = "localhost@test.com";
  //     this.data = {
  //       "AccountName": "Local Host",
  //       "DisplayName": "LocalHost:44307",
  //       "Email": "localhost@test.com",
  //       "ExtendedManagers": "No",
  //       "Title": "Titulo",
  //       "UserProfile_GUID": "",
  //       "UserName": undefined,
  //       "FirstName": undefined,
  //       "LastName": undefined,
  //       "ITESMPersonPIDM": undefined,
  //       "ITESMStAcadPlanID": undefined,
  //       "ITESMStAcadPlanDesc": undefined,
  //       "ITESMStACampusDesc": undefined,
  //       "ITESMStAcadPlanNivel": undefined,
  //       "ITESMStAcadPlanNivelDesc": undefined,
  //       "ITESMStAEstatus": undefined,
  //       "urlOrigen": window.location.href,
  //       "urlOrigenPadre": window.parent.window.location.href
  //     };
  //   }

  //   // Creando DirectLine
  //   this.token_resp = await this.call_api_directline(this.correo).toPromise();
  //   this.directLine = window.WebChat.createDirectLine({
  //     token: this.token_resp.token
  //   });

  //   // Creando DirectLineSpeech
  //   this.webSpeechPonyfillFactory = window.WebChat.createCognitiveServicesSpeechServicesPonyfillFactory({
  //    credentials: () => (this.get_credentials(this.call_api_speech(this.correo).toPromise()))
  //   });

  //   // Obteniendo estilos de adaptive cards
  //   const adaptiveCardsHostConfig = this.token_resp.adaptiveCardsHostConfig;
  //   // Programar el envío del evento para iniciar la conversación
  //   this.store = window.WebChat.createStore({}, ({ dispatch }) => next => action => {

  //     // Detectar cuando habla y cuando calla
  //     if (action.type === 'WEB_CHAT/MARK_ACTIVITY') {
  //       if (action.payload !== undefined) {
  //         if (action.payload.name == 'speak') {
  //           if (action.payload.value == true) {
  //             this.show_mute_mic();
  //             this.mute_cnt++;
  //           }
  //           else {
  //             this.mute_cnt--;

  //             // Deshabilitando el botón para evitar el bug
  //             if (document.getElementById('boton_mute')) {
  //               document.getElementById('boton_mute').setAttribute("disabled", "");
  //               document.getElementById('svg_mute_button').setAttribute('class', 'svg_mute_disabled');
  //             }

  //             document.getElementById('btn_chbot_cerrar') ? document.getElementById('btn_chbot_cerrar').setAttribute("disabled", "") : {};
  //             document.getElementById('btn_chbot_cerrar_movil') ? document.getElementById('btn_chbot_cerrar_movil').setAttribute("disabled", "") : {};
  //             // Esperando delay para volver a activar el botón
  //             const delay = timer(this.token_resp.delay);
  //             delay.subscribe(() => {
  //               if (document.getElementById('boton_mute')) {
  //                 document.getElementById('svg_mute_button').setAttribute('class', 'svg_mute');
  //                 document.getElementById('boton_mute').removeAttribute("disabled");
  //               }

  //               document.getElementById('btn_chbot_cerrar') ? document.getElementById('btn_chbot_cerrar').removeAttribute("disabled") : {};
  //               document.getElementById('btn_chbot_cerrar_movil') ? document.getElementById('btn_chbot_cerrar_movil').removeAttribute("disabled") : {};
  //             })

  //             if (this.mute_cnt < 0) {
  //               this.mute_cnt = 0
  //             }
  //             if (this.mute_cnt == 0) {
  //               document.getElementById('boton_mute').remove();
  //               this.boton_mute_creado = false;
  //               this.botWindowElement.nativeElement.querySelector('.webchat__icon-button').style.display = "inline";
  //             }
  //           }
  //         }
  //       }
  //     }

  //     // Iniciando la conversación del bot
  //     if (action.type === 'DIRECT_LINE/CONNECT_FULFILLED') {
  //       dispatch({
  //         type: 'WEB_CHAT/SEND_EVENT',
  //         payload: {
  //           name: 'startConversation'
  //         }
  //       });
  //     }

      // Cuando se pulsa un submit
//       if (action.type === 'WEB_CHAT/SEND_POST_BACK') {
//         if (action.payload.value.hasOwnProperty("key")) {
//           var value = action.payload.value;

//           // Enviando evento
//           dispatch({
//             type: 'WEB_CHAT/SEND_EVENT',
//             payload: {
//               name: 'evaluarMenu',
//               value: value
//             }
//           });

//           // Evaluando si contiene url
//           if (action.payload.value.hasOwnProperty("redirect_url")) {
//             var redirect_url = action.payload.value.redirect_url;
//             window.open(redirect_url, "_blank");
//           }
//         }
//       }
//       return next(action);
//     });



//     // Evaluar las iniciales que se mostrarán
//     let userAvatarInitials = undefined;
//     if (this.dominio != "localhost") {
//       userAvatarInitials = this.info["DisplayName"] != undefined ? this.info["DisplayName"].substring(0, 2).toUpperCase() : "XX"
//     }
//     else {
//       userAvatarInitials = "Usr"
//     }

//     // VERSION 2
//     const styleOptions = {
//       backgroundColor: '#FFFFFF',

//       // Caja de escritura
//       hideUploadButton: true,                               // Ocultar Clip de adjuntar
//       sendBoxBackground: 'rgb(232, 236, 237)',
//       sendBoxButtonColor: 'rgb(127, 127, 127)',               // Color del botón de envío
//       sendBoxButtonColorOnHover: "rgb(255, 51, 51)",
//       sendBoxTextColor: 'rgb(0, 0, 0) !important',                     // Color de texto de escritura
//       sendBoxPlaceholderColor: 'rgb(127, 127, 127) !important',              // Color de texto de ejemplo de la caja de escritura

//       // Burbujas de conversacion ::: BOT
//       bubbleBackground: 'linear-gradient(to bottom, rgb(208,226,248),  rgb(204,238,254));',
//       bubbleBorderRadius: 18,
//       bubbleBorderColor: 'rgb(205,209,209)',
//       bubbleBorderWidth: '1px',

//       // Burbujas de conversacion :::  USUARIO
//       bubbleFromUserBackground: 'rgb(252,253,253) !important',
//       bubbleFromUserBorderRadius: 18,
//       bubbleFromUserBorderColor: 'rgb(205,209,209)',
//       bubbleFromUserBorderWidth: '1px',

//       // Avatares
//       avatarSize: 35,
//       botAvatarBackgroundColor: "FFFFFF",
//       botAvatarImage: env.BOTconf.botAvatarImage,
//       botAvatarInitials: '',
//       userAvatarBackgroundColor: "#FFFFFF",
//       userAvatarImage: env.BOTconf.userAvatarImage,
//       userAvatarInitials: '',

//       bubbleNubOffset: 'bottom',
//       bubbleFromUserNubOffset: 'bottom',

//       // Mensaje de just now, sending...
//       subtle: 'rgb(0, 0, 0)',
//       typingAnimationBackgroundImage: "url('data:image/gif;base64,R0lGODlhXwAhAPEAAAAAACc6mUep1lin0yH5BAkKAAAAIf8LTkVUU0NBUEUyLjADAQAAACwAAAAAXwAhAAACz4SPqcvtD6OctNqLs968+w+G4kiW5ok2wcq2LvsMwkzX9jDE9l7j1gsENnC8Ii7HIBZ3x0rwCUsujciFbMqrSqDQBtbo/fKc3GBY3DujaeTyS432Mda2trsFj+fR9vtq/6WVQFfT5weIJYhAyEbh54K4JLfAOGN4FymZuXTptpn1Oeb4GEXJqHhQKdBZFtqDaqDKyuWaNlc523VLCAsgO0oaUEvT+zsR/Ce11qS83MuArBP3DHAlNgl86KAUCMGdmBIuPk5ebn6Onq6+zp5RAAAh+QQJCgAAACwAAAAAXwAhAAAC0ISPqcvtD6OctNqLs968+w+G4kiW5ok2g8C27jsMT0DX9l0/68u3sRXrCWMyBu54VO2EsB+FyXQukNQcYwmFVbLDRrXa4Aq3Yq3xiwyXX+S1QKpApxluV9tdnMpx6rrgvpYXt3fTVwdYJphAWEjn9/dUB7fIaLXwCDnxqIhQaamAiSjGeeBJY+gmykVqYBqAuqYKNdlpClsmO+tl6+iXG7XreSv223Um3HtIgbVKW8q7wNwMFMjaCh1dfRE0CoGslJgiPk5ebn6Onq6+zt6eUQAAIfkECQoAAAAsAAAAAF8AIQAAAtCEj6nL7Q+jnLTai7PevPsPhuJIluaJNoPAtu47DM/61m38BPrO97sVswljMkZQWCM2fExmBYnEGaE26aKJ/VGowwa3usxin1+Yt+wqXsVNMlpgVbxZ8QS7vZ3Dz3O14u6U18f35mcH2ONWSIhmiICYKNjoeKBX9wipNaG3x8BJaZCpKfHJWAYKIKqj2Gj6haoawPqKagmrOvvqynUrmsu7S9Wb+SscDDUMWUFzennATJuMCDQ5U+2AuwyNDHEEnEOcIj5OXm5+jp6uvs7eflEAADs=')",
//       typingAnimationHeight: 33,
//       typingAnimationWidth: 95,
//     }

//     // Enviando información del usuario y renderizando el bot
//     window.WebChat.renderWebChat(
//       {
//         userID: this.token_resp.user_id,
//         username: this.data["Email"],

//         directLine : new DirectLine({
//           token: this.token_resp.token,
//           conversationStartProperties: {
//               locale: localStorage.localeBot
//           },
//         }),
        
//         store: this.store,
//         adaptiveCardsHostConfig: adaptiveCardsHostConfig,
//         styleOptions: styleOptions,
//         locale: localStorage.localeBot,
//         webSpeechPonyfillFactory: this.webSpeechPonyfillFactory,
        
//         selectVoice: ( voices, activity ) => 
//           activity.locale === 'es-mx' ?
//           voices.find( ( { name } ) => /DaliaNeural/iu.test( name ) )
//           :
//           voices.find( ( { name } ) => /AriaNeural/iu.test( name ) )
//       }, this.botWindowElement.nativeElement
//     );

//     // Cambiando flecha
//     if (!this.token_resp.speech_on) {
//       this.botWindowElement.nativeElement.querySelector('.webchat__send-icon').setAttribute("viewBox", "0 0 30 20");
//       this.botWindowElement.nativeElement.querySelector('.webchat__send-icon > path').setAttribute("d", this.svg_send);
//     }
//   }

//   enviar_evento_stop_speech() {
//     this.store.dispatch({
//       type: 'WEB_CHAT/STOP_SPEAKING'
//     });
//   }

//   generar_evento() {
//     var event = new CustomEvent('bot_stop_speech');
//     document.dispatchEvent(event);
//   }

//   show_mute_mic() {
//     if (this.boton_mute_creado == false) {

//       // Creación de elemento
//       var boton_mute = document.createElement("button");
//       boton_mute.id = "boton_mute";
//       boton_mute.style.cssText = "background-color: white; border-top-right-radius: 20px; border-bottom-right-radius: 20px; border: none;";
//       boton_mute.innerHTML = '<svg id="svg_mute_button" class="svg_mute" height="28" viewBox="0 0 34.75 46" width="28"><path class="a" d="m 29.75 23 l 0 6.36 a 7 7 0 0 1 -0.56 2.78 a 7.16 7.16 0 0 1 -3.8 3.8 a 7 7 0 0 1 -2.78 0.56 l -4.11 0 l 0 2.25 l 4.5 0 l 0 2.25 l -11.25 0 l 0 -2.25 l 4.5 0 l 0 -2.25 l -4.11 0 a 7 7 0 0 1 -2.78 -0.56 a 7.16 7.16 0 0 1 -3.8 -3.8 a 7 7 0 0 1 -0.56 -2.78 l 0 -6.36 l 2.25 0 l 0 6.36 a 4.72 4.72 0 0 0 0.39 1.9 a 4.78 4.78 0 0 0 2.6 2.6 a 4.72 4.72 0 0 0 1.9 0.39 l 10.47 0 a 4.72 4.72 0 0 0 1.9 -0.39 a 4.78 4.78 0 0 0 2.6 -2.6 a 4.72 4.72 0 0 0 0.39 -1.9 l 0 -6.36 l 2.25 0 z m -18 5.62 a 1.13 1.13 0 0 0 1.13 1.13 l 9 0 a 1.13 1.13 0 0 0 1.12 -1.13 l 0 -20.24 a 1.13 1.13 0 0 0 -1.12 -1.13 l -9 0 a 1.13 1.13 0 0 0 -1.13 1.13 l 0 20.24 z m 1.13 3.38 a 3.41 3.41 0 0 1 -1.32 -0.26 a 3.31 3.31 0 0 1 -1.8 -1.8 a 3.41 3.41 0 0 1 -0.26 -1.32 l 0 -20.24 a 3.41 3.41 0 0 1 0.26 -1.32 a 3.31 3.31 0 0 1 1.8 -1.8 a 3.41 3.41 0 0 1 1.32 -0.26 l 9 0 a 3.4 3.4 0 0 1 1.31 0.26 a 3.31 3.31 0 0 1 1.8 1.8 a 3.41 3.41 0 0 1 0.26 1.32 l 0 20.24 a 3.41 3.41 0 0 1 -0.26 1.32 a 3.31 3.31 0 0 1 -1.8 1.8 a 3.4 3.4 0 0 1 -1.31 0.26 l -9 0 z m -8.81 -26.36 l 1.86 -1.28 l 23.75 36 l -1.86 1.28 z"></path></svg>'
//       boton_mute.onclick = this.generar_evento;

//       // Insertar elemento
//       var speech_button = this.botWindowElement.nativeElement.querySelector('.webchat__icon-button');
//       speech_button.style.display = "none";
//       speech_button.parentNode.insertBefore(boton_mute, speech_button.nextSibling);

//       this.boton_mute_creado = true;
//     }
//   }

//   expireAfter = 0
//   async get_credentials(call_api_speech) {
//     const now = Date.now();
//     if (now > this.expireAfter) {
//       const minutesToAdd = this.token_resp.minutesToAdd
//       this.expireAfter = now + minutesToAdd * 60000;
//       this.credentials = await call_api_speech;
//     }
//     return this.credentials
//   }
// }
