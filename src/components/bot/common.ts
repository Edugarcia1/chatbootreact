export const DominiosAPI = {
    dev: "https://mkt-test-20220225-webapi.azurewebsites.net",
    pprd: "https://mkt-test-20220225-webapi.azurewebsites.net",
    prod: "https://mkt-test-20220225-webapi.azurewebsites.net"
}

export const BOTconf = {
    Sharepoint_API: (DominioSharepoint: string) => `https://${DominioSharepoint}/_api/SP.UserProfiles.PeopleManager/GetMyProperties`,
    DL_SecretToToken: (DominioAPI: string) => `${DominioAPI}/DirectLine`,
    DL_SpeechToken: (DominioAPI: string) => `${DominioAPI}/DirectLineSpeech`,
    botAvatarImage: "https://stg0chbot0tec0prod.blob.core.windows.net/bot-imagen/BotImage.png",
    userAvatarImage: "https://stg0chbot0tec0prod.blob.core.windows.net/bot-imagen/Pug.png",
    // Probablemente no sean URL sino url('data:image/gif;base64...)
    typingAnimationBackgroundImage: "",
    spinnerAnimationBackgroundImage: ""
}
