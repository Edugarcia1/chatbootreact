import { useNavigate} from 'react-router-dom';
import { DirectLine } from 'botframework-directlinejs';
import * as env from './bot/common';

import '../index.css';

export function ChatBoot( )   {

console.log("Aqui empieza la funcion CHATBOOT");

let store = undefined;
let directLine = undefined;


const navigate = useNavigate();

// Obteniendo estilos de adaptive cards
const adaptiveCardsHostConfig = this.token_resp.adaptiveCardsHostConfig;

// VERSION 2    
const styleOptions = {
            backgroundColor: '#FFFFFF',
      
            // Caja de escritura
            hideUploadButton: true,                               // Ocultar Clip de adjuntar
            sendBoxBackground: 'rgb(232, 236, 237)',
            sendBoxButtonColor: 'rgb(127, 127, 127)',               // Color del botón de envío
            sendBoxButtonColorOnHover: "rgb(255, 51, 51)",
            sendBoxTextColor: 'rgb(0, 0, 0) !important',                     // Color de texto de escritura
            sendBoxPlaceholderColor: 'rgb(127, 127, 127) !important',              // Color de texto de ejemplo de la caja de escritura
      
            // Burbujas de conversacion ::: BOT
            bubbleBackground: 'linear-gradient(to bottom, rgb(208,226,248),  rgb(204,238,254));',
            bubbleBorderRadius: 18,
            bubbleBorderColor: 'rgb(205,209,209)',
            bubbleBorderWidth: '1px',
      
            // Burbujas de conversacion :::  USUARIO
            bubbleFromUserBackground: 'rgb(252,253,253) !important',
            bubbleFromUserBorderRadius: 18,
            bubbleFromUserBorderColor: 'rgb(205,209,209)',
            bubbleFromUserBorderWidth: '1px',
      
            // Avatares
            avatarSize: 35,
            botAvatarBackgroundColor: "FFFFFF",
            botAvatarImage: env.BOTconf.botAvatarImage,
            botAvatarInitials: '',
            userAvatarBackgroundColor: "#FFFFFF",
            userAvatarImage: env.BOTconf.userAvatarImage,
            userAvatarInitials: '',
      
            bubbleNubOffset: 'bottom',
            bubbleFromUserNubOffset: 'bottom',
      
            // Mensaje de just now, sending...
            subtle: 'rgb(0, 0, 0)',
            typingAnimationBackgroundImage: "url('data:image/gif;base64,R0lGODlhXwAhAPEAAAAAACc6mUep1lin0yH5BAkKAAAAIf8LTkVUU0NBUEUyLjADAQAAACwAAAAAXwAhAAACz4SPqcvtD6OctNqLs968+w+G4kiW5ok2wcq2LvsMwkzX9jDE9l7j1gsENnC8Ii7HIBZ3x0rwCUsujciFbMqrSqDQBtbo/fKc3GBY3DujaeTyS432Mda2trsFj+fR9vtq/6WVQFfT5weIJYhAyEbh54K4JLfAOGN4FymZuXTptpn1Oeb4GEXJqHhQKdBZFtqDaqDKyuWaNlc523VLCAsgO0oaUEvT+zsR/Ce11qS83MuArBP3DHAlNgl86KAUCMGdmBIuPk5ebn6Onq6+zp5RAAAh+QQJCgAAACwAAAAAXwAhAAAC0ISPqcvtD6OctNqLs968+w+G4kiW5ok2g8C27jsMT0DX9l0/68u3sRXrCWMyBu54VO2EsB+FyXQukNQcYwmFVbLDRrXa4Aq3Yq3xiwyXX+S1QKpApxluV9tdnMpx6rrgvpYXt3fTVwdYJphAWEjn9/dUB7fIaLXwCDnxqIhQaamAiSjGeeBJY+gmykVqYBqAuqYKNdlpClsmO+tl6+iXG7XreSv223Um3HtIgbVKW8q7wNwMFMjaCh1dfRE0CoGslJgiPk5ebn6Onq6+zt6eUQAAIfkECQoAAAAsAAAAAF8AIQAAAtCEj6nL7Q+jnLTai7PevPsPhuJIluaJNoPAtu47DM/61m38BPrO97sVswljMkZQWCM2fExmBYnEGaE26aKJ/VGowwa3usxin1+Yt+wqXsVNMlpgVbxZ8QS7vZ3Dz3O14u6U18f35mcH2ONWSIhmiICYKNjoeKBX9wipNaG3x8BJaZCpKfHJWAYKIKqj2Gj6haoawPqKagmrOvvqynUrmsu7S9Wb+SscDDUMWUFzennATJuMCDQ5U+2AuwyNDHEEnEOcIj5OXm5+jp6uvs7eflEAADs=')",
            typingAnimationHeight: 33,
            typingAnimationWidth: 95,
}


    //Aqui inicia con la funcion start();
    function iniciaChat() {
        console.log('Inicia...BOX*CHAT, debe entrar a la funcion INICIACHAT');
        alert('INICIA CHAT');

        // Enviando información del usuario y renderizando el bot
        // window.WebChat.renderWebChat(
        //     {
        //     userID: this.token_resp.user_id,
        //     username: this.data["Email"],
    
        //     directLine : new DirectLine({
        //         token: this.token_resp.token,
        //         conversationStartProperties: {
        //             locale: localStorage.localeBot
        //         },
        //     }),
            
        //     store: this.store,
        //     adaptiveCardsHostConfig: adaptiveCardsHostConfig,
        //     styleOptions: styleOptions,
        //     locale: localStorage.localeBot,
        //     webSpeechPonyfillFactory: this.webSpeechPonyfillFactory,
            
        //     selectVoice: ( voices, activity ) => 
        //         activity.locale === 'es-mx' ?
        //         voices.find( ( { name } ) => /DaliaNeural/iu.test( name ) )
        //         :
        //         voices.find( ( { name } ) => /AriaNeural/iu.test( name ) )
        //     }, this.botWindowElement.nativeElement
        // );

        // //Siguiente funcion para iniciar el chat
        // (async function () {
        //     const res = await fetch('https://webchat-mockbot.azurewebsites.net/directline/token', { method: 'POST' });
        //     const { token } = await res.json();
        //     window.WebChat.renderWebChat({
        //         directLine: window.WebChat.createDirectLine({ token })
        //     }, document.getElementById('webchat'));
        // })();
        // open();
    }
    // const navigate= useNavigate();

    const cambiarIdioma = () =>{
        console.log("Para cambiar idioma");
        if(localStorage.localeBot === 'es-mx') {
        localStorage.localeBot = 'en-us'
        this.changeLocale = 'ES'
        this.bot.start()
        } else {
        localStorage.localeBot = 'es-mx'
        this.changeLocale = 'EN'
        this.bot.start()
        }
    }

    const openDialogCaritometro = () =>{
        console.log("Para Dialogo Carimetro");
        /*this.dialog.open(CaritometroChatBotComponent, {
        maxHeight: '600px',
        maxWidth: '700px'
        });*/
    }

    const close = () =>{
        console.log("Para cerrar el boot");
        navigate(-1);
        // navigate('/',{
        //     replace:true
        // });

        // Cierra el bot 
        this.bot.enviar_evento_stop_speech();

        //Se habilita nuevamente el botón de llamada al bot.
        this.saludoElement.nativeElement.style.opacity = 1;
        this.saludoElement.nativeElement.style.visibility = "visible";
        this.mostrar_saludo = !this.mostrar_saludo;

        //Contenedor del chat
        this.chatElement.nativeElement.style.opacity = 0;
        this.chatElement.nativeElement.style.visibility = "hidden";

        // Encabezado
        this.encabezadoElement.nativeElement.style.opacity = 0;
        this.encabezadoElement.nativeElement.style.visibility = "hidden";

        // Cuerpo del bot
        this.bot.botWindowElement.nativeElement.style.opacity = 0;
        this.bot.botWindowElement.nativeElement.style.visibility = "hidden";
    }

    return (
        <>
            <body>
                <div className="floating-chat">
                    <div className="encabezado">
                        <div className='left'>
                            <img className="btn_imagenbot"/><span>&nbsp;&nbsp;&nbsp;<b>TECbot</b></span>
                        </div>
                        <div className='right'>
                            <button  className="btn_translate" onClick={cambiarIdioma}>
                                    <p style="display:inline-block; vertical-align:middle" className="material-icons-translate"></p>
                                    <p style="display:inline-block; vertical-align:middle" className="material-icons-translate"></p>
                            </button>
                            <button className="btn_encabezado" onClick={openDialogCaritometro}>
                                    <i style="display:inline-block; vertical-align:middle" className="material-icons">tag_faces</i>
                            </button>
                            <button className="btn_encabezado" onClick={close} id="btn_chbot_cerrar">
                                    <i style="display:inline-block; vertical-align:middle" className="material-icons">close</i>
                            </button>
                        </div>
                        <div className="webchat" role="main">{iniciaChat}</div>
                        {/* <div className="webchat" role="main" /> */}
                    </div>   
                </div>
            </body>
            
        </>      
    )
       
}

// export {ChatBoot} 
// export  {ChatBoot};