
import axios from 'axios';
import * as env from './common'
import { Observable, timer } from 'rxjs';
import { DirectLine } from 'botframework-directlinejs';
import { useDispatch } from 'react-redux';

import {CallApiSpeech} from './CallApiSpeech';
import {CallApiDirectline} from './CallApiDirectline';


declare global {
  interface Window {
    WebChat: any;
  }
}
window.WebChat = window.WebChat || {};

// constructor(private http: HttpClient) { }
let info = undefined;
let data: any;
let dominio : string;
let dominioAPI: string;
let developer = false;
let svg_send = "M2.01,3 L2,10 L17,12 L2,14 L2.01,21 L23,12 L2.01,3 Z M4,8.25 L4,6.03 L11.51,9.25 L4,8.25 Z M4.01,17.97 L4.01,15.75 L11.52,14.75 L4.01,17.97 Z";
let disp_movil : boolean;

// Otras variables globales
let correo : any;
let token_resp : any;
let store : any;
let directLine = undefined;
let credentials : any;
let webSpeechPonyfillFactory = undefined;
let mute_cnt = 0;
let boton_mute_creado = false;

// Mandamos a llamar las apis
let call_api_speech = CallApiSpeech;
let call_api_directline = CallApiDirectline;


// function checkOrientation() {
//     let altura_pantalla = screen.height;
//     console.log("###>>> Altura pantalla: " + altura_pantalla)
//     let altura_enc_navegador = screen.height - window.innerHeight;
//     let altura_env_mitec;

//     // Validando que contenga TAG NAV
//     if (document.getElementsByTagName("nav").length == 0) {
//       altura_env_mitec = 72;
//     }
//     else {
//       altura_env_mitec = document.getElementsByTagName("nav")[0].offsetHeight;
//     }

//     // Ajustando para dispositivos
//     if (disp_movil == true) {
//       // altura_env_mitec = 110;  // dispositivo movil
//       altura_env_mitec = altura_env_mitec + 35;  // dispositivo movil
//     }
//     else {
//       // altura_env_mitec = 130;  // dispositivo escritorio
//       altura_env_mitec = altura_env_mitec + 60;  // escritorio
//     }
//     let altura_calc = altura_pantalla - (altura_enc_navegador + altura_env_mitec);
//     // console.log("###>>> Altura calculada: " + altura_calc)
//     if (altura_calc > 600) {
//       altura_calc = 600
//     }
//     let str_calc = altura_calc.toString() + "px";
//     // botWindowElement.nativeElement.style.height = str_calc;
//     // document.getElementById('webchatbot').style.height = str_calc;
// }

async function call_api() {
    // Llamar la api de sharepoint para obtener los datos del usuario
    // Formando Api Sharepoint
    let api = env.BOTconf.Sharepoint_API(dominio);
    //  Llamado a API por medio de una Promise
    const promise = new Promise((resolve, reject) => {
      axios.get(api)
          .then(res => {
              resolve(res)
          })
          .catch(err => reject(err))
           //Revisa error en consola
        // console.log("Promise rejected with " + JSON.stringify(err));
    })
    // Llamado a API por medio de una Promise
    // const promise = this.http.get(api).toPromise();

    // await promise.then((data) => {
    //   this.info = data;
    //   // Construyendo el json de envío
    //   this.build_data(this.info)
    // }).catch((error) => {
    //   //Revisa error en consola
    //   console.log("Promise rejected with " + JSON.stringify(error));
    // });
}


let expireAfter = 0
async function get_credentials(call_api_speech:any) {
    const now = Date.now();
    var credentials = null;
    if (now > expireAfter) {
      const minutesToAdd = token_resp.minutesToAdd
      expireAfter = now + minutesToAdd * 60000;
      credentials = await call_api_speech;
    }
    return credentials;
}


async function build_bot() {
    //############################################################################################
    if (developer == false) {
      await call_api();
      correo = data.Email;
    }
    else {
      // En caso de que se corra localmente
      // ng serve --port 44307 --ssl
      correo = "localhost@test.com";
      data = {
        "AccountName": "Local Host",
        "DisplayName": "LocalHost:44307",
        "Email": "localhost@test.com",
        "ExtendedManagers": "No",
        "Title": "Titulo",
        "UserProfile_GUID": "",
        "UserName": undefined,
        "FirstName": undefined,
        "LastName": undefined,
        "ITESMPersonPIDM": undefined,
        "ITESMStAcadPlanID": undefined,
        "ITESMStAcadPlanDesc": undefined,
        "ITESMStACampusDesc": undefined,
        "ITESMStAcadPlanNivel": undefined,
        "ITESMStAcadPlanNivelDesc": undefined,
        "ITESMStAEstatus": undefined,
        "urlOrigen": window.location.href,
        "urlOrigenPadre": window.parent.window.location.href
      };
    }

    // Creando DirectLine
    token_resp = await call_api_directline(correo);
    directLine = window.WebChat.createDirectLine({
      token: token_resp.token
    });

    // Creando DirectLineSpeech
    webSpeechPonyfillFactory = window.WebChat.createCognitiveServicesSpeechServicesPonyfillFactory({
     credentials: () => (get_credentials(call_api_speech(correo)))
    });

    // Obteniendo estilos de adaptive cards
    const adaptiveCardsHostConfig = token_resp.adaptiveCardsHostConfig;
    // Programar el envío del evento para iniciar la conversación
    // store = window.WebChat.createStore({}, ({ dispatch }) => next => action => {

    //   // Detectar cuando habla y cuando calla
    //   // if (action.type === 'WEB_CHAT/MARK_ACTIVITY') {
    //   //   if (action.payload !== undefined) {
    //   //     if (action.payload.name == 'speak') {
    //   //       if (action.payload.value == true) {
    //   //         this.show_mute_mic();
    //   //         this.mute_cnt++;
    //   //       }
    //   //       else {
    //   //         this.mute_cnt--;

    //   //         // Deshabilitando el botón para evitar el bug
    //   //         if (document.getElementById('boton_mute')) {
    //   //           document.getElementById('boton_mute').setAttribute("disabled", "");
    //   //           document.getElementById('svg_mute_button').setAttribute('class', 'svg_mute_disabled');
    //   //         }

    //   //         document.getElementById('btn_chbot_cerrar') ? document.getElementById('btn_chbot_cerrar').setAttribute("disabled", "") : {};
    //   //         document.getElementById('btn_chbot_cerrar_movil') ? document.getElementById('btn_chbot_cerrar_movil').setAttribute("disabled", "") : {};
    //   //         // Esperando delay para volver a activar el botón
    //   //         const delay = timer(token_resp.delay);
    //   //         delay.subscribe(() => {
    //   //           if (document.getElementById('boton_mute')) {
    //   //             document.getElementById('svg_mute_button').setAttribute('class', 'svg_mute');
    //   //             document.getElementById('boton_mute').removeAttribute("disabled");
    //   //           }

    //   //           document.getElementById('btn_chbot_cerrar') ? document.getElementById('btn_chbot_cerrar').removeAttribute("disabled") : {};
    //   //           document.getElementById('btn_chbot_cerrar_movil') ? document.getElementById('btn_chbot_cerrar_movil').removeAttribute("disabled") : {};
    //   //         })

    //   //         if (mute_cnt < 0) {
    //   //           mute_cnt = 0
    //   //         }
    //   //         if (mute_cnt == 0) {
    //   //           document.getElementById('boton_mute').remove();
    //   //           boton_mute_creado = false;
    //   //           this.botWindowElement.nativeElement.querySelector('.webchat__icon-button').style.display = "inline";
    //   //         }
    //   //       }
    //   //     }
    //   //   }
    //   // }

    //   // Iniciando la conversación del bot
    //   if (action.type === 'DIRECT_LINE/CONNECT_FULFILLED') {
    //     dispatch({
    //       type: 'WEB_CHAT/SEND_EVENT',
    //       payload: {
    //         name: 'startConversation'
    //       }
    //     });
    //   }

    //   // Cuando se pulsa un submit
    //   if (action.type === 'WEB_CHAT/SEND_POST_BACK') {
    //     if (action.payload.value.hasOwnProperty("key")) {
    //       var value = action.payload.value;

    //       // Enviando evento
    //       dispatch({
    //         type: 'WEB_CHAT/SEND_EVENT',
    //         payload: {
    //           name: 'evaluarMenu',
    //           value: value
    //         }
    //       });

    //       // Evaluando si contiene url
    //       if (action.payload.value.hasOwnProperty("redirect_url")) {
    //         var redirect_url = action.payload.value.redirect_url;
    //         window.open(redirect_url, "_blank");
    //       }
    //     }
    //   }
    //   return next(action);
    // });



    // Evaluar las iniciales que se mostrarán
    let userAvatarInitials = undefined;
    // if (dominio != "localhost") {
    //   userAvatarInitials = this.info["DisplayName"] != undefined ? this.info["DisplayName"].substring(0, 2).toUpperCase() : "XX"
    // }
    // else {
    //   userAvatarInitials = "Usr"
    // }

    // VERSION 2
    const styleOptions = {
      backgroundColor: '#FFFFFF',

      // Caja de escritura
      hideUploadButton: true,                               // Ocultar Clip de adjuntar
      sendBoxBackground: 'rgb(232, 236, 237)',
      sendBoxButtonColor: 'rgb(127, 127, 127)',               // Color del botón de envío
      sendBoxButtonColorOnHover: "rgb(255, 51, 51)",
      sendBoxTextColor: 'rgb(0, 0, 0) !important',                     // Color de texto de escritura
      sendBoxPlaceholderColor: 'rgb(127, 127, 127) !important',              // Color de texto de ejemplo de la caja de escritura

      // Burbujas de conversacion ::: BOT
      bubbleBackground: 'linear-gradient(to bottom, rgb(208,226,248),  rgb(204,238,254));',
      bubbleBorderRadius: 18,
      bubbleBorderColor: 'rgb(205,209,209)',
      bubbleBorderWidth: '1px',

      // Burbujas de conversacion :::  USUARIO
      bubbleFromUserBackground: 'rgb(252,253,253) !important',
      bubbleFromUserBorderRadius: 18,
      bubbleFromUserBorderColor: 'rgb(205,209,209)',
      bubbleFromUserBorderWidth: '1px',

      // Avatares
      avatarSize: 35,
      botAvatarBackgroundColor: "FFFFFF",
      botAvatarImage: env.BOTconf.botAvatarImage,
      botAvatarInitials: '',
      userAvatarBackgroundColor: "#FFFFFF",
      userAvatarImage: env.BOTconf.userAvatarImage,
      userAvatarInitials: '',

      bubbleNubOffset: 'bottom',
      bubbleFromUserNubOffset: 'bottom',

      // Mensaje de just now, sending...
      subtle: 'rgb(0, 0, 0)',
      typingAnimationBackgroundImage: "url('data:image/gif;base64,R0lGODlhXwAhAPEAAAAAACc6mUep1lin0yH5BAkKAAAAIf8LTkVUU0NBUEUyLjADAQAAACwAAAAAXwAhAAACz4SPqcvtD6OctNqLs968+w+G4kiW5ok2wcq2LvsMwkzX9jDE9l7j1gsENnC8Ii7HIBZ3x0rwCUsujciFbMqrSqDQBtbo/fKc3GBY3DujaeTyS432Mda2trsFj+fR9vtq/6WVQFfT5weIJYhAyEbh54K4JLfAOGN4FymZuXTptpn1Oeb4GEXJqHhQKdBZFtqDaqDKyuWaNlc523VLCAsgO0oaUEvT+zsR/Ce11qS83MuArBP3DHAlNgl86KAUCMGdmBIuPk5ebn6Onq6+zp5RAAAh+QQJCgAAACwAAAAAXwAhAAAC0ISPqcvtD6OctNqLs968+w+G4kiW5ok2g8C27jsMT0DX9l0/68u3sRXrCWMyBu54VO2EsB+FyXQukNQcYwmFVbLDRrXa4Aq3Yq3xiwyXX+S1QKpApxluV9tdnMpx6rrgvpYXt3fTVwdYJphAWEjn9/dUB7fIaLXwCDnxqIhQaamAiSjGeeBJY+gmykVqYBqAuqYKNdlpClsmO+tl6+iXG7XreSv223Um3HtIgbVKW8q7wNwMFMjaCh1dfRE0CoGslJgiPk5ebn6Onq6+zt6eUQAAIfkECQoAAAAsAAAAAF8AIQAAAtCEj6nL7Q+jnLTai7PevPsPhuJIluaJNoPAtu47DM/61m38BPrO97sVswljMkZQWCM2fExmBYnEGaE26aKJ/VGowwa3usxin1+Yt+wqXsVNMlpgVbxZ8QS7vZ3Dz3O14u6U18f35mcH2ONWSIhmiICYKNjoeKBX9wipNaG3x8BJaZCpKfHJWAYKIKqj2Gj6haoawPqKagmrOvvqynUrmsu7S9Wb+SscDDUMWUFzennATJuMCDQ5U+2AuwyNDHEEnEOcIj5OXm5+jp6uvs7eflEAADs=')",
      typingAnimationHeight: 33,
      typingAnimationWidth: 95,
    }

    // Enviando información del usuario y renderizando el bot
    window.WebChat.renderWebChat(
      {
        userID: token_resp.user_id,
        username: data.Email,

        directLine : new DirectLine({
          token: token_resp.token,
          conversationStartProperties: {
              locale: localStorage.localeBot
          },
        }),
        
        store: store,
        adaptiveCardsHostConfig: adaptiveCardsHostConfig,
        styleOptions: styleOptions,
        locale: localStorage.localeBot,
        webSpeechPonyfillFactory: webSpeechPonyfillFactory,
        
        // selectVoice: ( voices:any, activity:any ) => 
        //   activity.locale === 'es-mx' ?
          // voices.find( ( { name } ) => /DaliaNeural/iu.test( name ) )
          // :
          // voices.find( ( { name } ) => /AriaNeural/iu.test( name ) )
        // }, this.botWindowElement.nativeElement
      }, document.getElementById('webchatbot')
    );

    // Cambiando flecha
    if (!token_resp.speech_on) {

      // document.getElementById('webchatbot').querySelector('.webchat__send-icon').setAttribute("viewBox", "0 0 30 20");
      // // this.botWindowElement.nativeElement.
      // this.document.getElementById('webchatbot').querySelector('.webchat__send-icon > path').setAttribute("d", this.svg_send);
    }
}